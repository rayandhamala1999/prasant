/* 

DAY-1
=======
npx create-react-app this is the command for to init the default app packages in react  

after clone we need to do npm i 


multi line comments => alt+shift+A

a react variable can store tag 

we can use implement javascript operation inside tag using {}
there must be only one wrapper in return of that function in app.js 
any thing that is written inside return will display in the browser 

block element= takes full space and start with new line eg: <div> <p> <h1>

inline element= takes required width eg: <span>

every tag is both opening and closing

Image 
always place images or the files at public folder 
. at src of image refers to public folder 

CSS
inline
    obj is used for styling purpose
    you must use camel convention 

external 
      it is 3 step process 
      define at separate file 
      import into index.js
      use everywhere 

inspect =ctrl+shift+i
in react console will appear at developer panel 

DAY-2
=======

html tag props and children 

component: is a function whose first letter is capital because of custom tag not match with inbuilt tag 
we can call component like as calling html tag

props: if we pass the value in component called as props 
(put can update and overwrite all the values so it is time consuming ) and patch will update only specific type of input data  

if props is other than string wrap it by curly braces{}

components are the custom tag 

inbuilt props(className,style) are only support  inbuilt tag(html tag ) not supported in custom tag (component)

DAY-3
=======
.jsx= is javascript extension 
it is the combination of the javascript with html  

limitation of {}curly braces 
 =>it must return only one value 
 =>it doesn't support for if else,for,while,do-while
 =>we cannot define variable inside curly braces 


limitation of ternary operator : you must need to write else part after if with  the (:)

effect of different data inside html tag
==>Boolean are not shown in browser we have to add some logic with condition within it 
don't call object inside  html tag children 



Functions of the UseState in react 

 a page will render when the state variable is change 

 when state variable change
 A component gets render such that
 the state variable which is changed holds changed value
 where as other state variable holds previous value 

 UseEffect function 
 it is asynchronous function 
 and it will execute at the last 
 useEffect function only execute in first render 
 useEffect function will run for the first render but from the second render the execution of the function depends on its dependency 

Explain the lIfe cycle of the Component 
=>Component did mount (first render )
useEffect fun will run 
=>Component did update(2nd render ) when state variable change 
useEffect fun will run only if its dependency changes 
=>Component did unmount (component removed )
nothing gets execute 
but clean up function execute 


before working with react router dom you have to wrap app component by bowse router in index 

form part of react

in input whatever you place in a value it will display in the browser <input>
 a tag refresh all page

 alt+shift+F is used to maintain proper alignment on code in VS

 navigate  and setState should be used only inside the useEffect or inside the button
*/