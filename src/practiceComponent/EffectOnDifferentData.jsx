import React from 'react'

const EffectOnDifferentData = () => {
    let name = "prasant";
    let age = 22;
    let isMarried = false;
    let favFood = [<div style={{color:"red"}}>yomari</div>, <div>chicken</div>];
    let info = { address: "tarkeshwor", fatherName: "lalPrasad" };

  return (
    <div>
        name is {name}
        <br></br>
        age is {age} 
        <br></br>
        Are you married?{isMarried?"Yes":"No"}
         <br></br>
        {favFood}
        <br></br>
        {info.fatherName}
        
    </div>
  )
}

export default EffectOnDifferentData
