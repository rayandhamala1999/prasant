import React, { useState } from 'react'

//define variable using useState
const LearnUseStateHook1 = () => {
    let [name,setName]=useState("prasant") 
    let [age,setAge]=useState(20)
                                                  
     let handleClick=(e)=>{
        setName("Ram")
       
     }
     let handleAge=(e)=>{
        setAge(22)
     }
  return (
    <div>
my name is {name}
<button onClick={handleClick}>Change Name</button> <br></br>
my age is {age}
<button onClick={handleAge}>Change Age</button>
    </div>
  )
}

export default LearnUseStateHook1