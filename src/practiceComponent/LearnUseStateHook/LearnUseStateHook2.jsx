import React, { useState } from 'react'

//define variable using useState
const LearnUseStateHook2 = () => {
    let [count,setCount]=useState(0) 
    
                                                  
     let handleIncrement=(e)=>{
        if(count<10){ 
       setCount(count+1)
        }
       
     }
    let decrement=(e)=>{
        if(count>0){
        setCount(count-1)}
    }
     let reset=(e)=>{
        setCount(0)
     }
  return (
    <div>
count is  {count}
<button onClick={handleIncrement}>incremented</button> <br></br>
<button onClick={decrement}>decrement</button> <br></br>
<button onClick={reset}>reset</button> <br></br>
    </div>
  )
  }

export default LearnUseStateHook2