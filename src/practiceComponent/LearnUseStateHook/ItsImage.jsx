import { isVisible } from '@testing-library/user-event/dist/utils'
import React, { useState } from 'react'

const ItsImage = () => {
    let [showImg,setShowImg]=useState(true) 
                                                   
    // let handleShow=(e)=>{
    //     setShowImg(true)

    // }
    // let handleHide=(e)=>{
    //     setShowImg(false)

    // }

let handleImg=(isVisible)=>{
    return(e)=>{
        setShowImg(isVisible)
    }
}
  
  return (
    <div>
{showImg?<img src="./logo192.png" alt="logo"></img>:null}<br></br>
<button onClick={handleImg(true)}>Show</button> <br></br>
<button onClick={handleImg(false)}>Hide</button> <br></br>
    </div>
  )
}

export default ItsImage



//handelImg (e)=>{ }                if you don't need to pass value
//handleImg ()=>{return ((e)=>{})}  if you need to pass the value 