import React, { useState } from 'react'

const WhyUseState = () => {
    let [name,setName]=useState("Maxwell")
  return (
    <div>
{name}
<button onClick={()=>{setName("AlexGender")}}>Change name</button>
    </div>
  )
}

export default WhyUseState