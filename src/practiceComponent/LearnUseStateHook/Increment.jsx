import React, { useState } from 'react'

const Increment = () => {
    let[count,setCount]=useState(0)
    let[count2,setCount2]=useState(100)
    
    let handleCount=(e)=>{
setCount(count+1)
    }
    let handleIncrement1=(e)=>{
        setCount2(count2+1)
    }
  return (
    <div> 
        
        count is{count} 
        <br></br> count2 is {count2}<br></br>
    <button onClick={handleCount}> Increment</button><br></br>
    <button onClick={handleIncrement1}> Increment1</button>
    
    </div>
  )
}

export default Increment