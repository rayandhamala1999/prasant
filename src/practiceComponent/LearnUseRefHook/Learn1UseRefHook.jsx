import React, { useRef } from "react";

const Learn1UseRefHook = () => {
  let ref1 = useRef();
  let ref2 = useRef();
  let refInput = useRef();

  return (
    <div>
      <div
        onClick={(e) => {
          refInput.current.focus();
        }}
      >
        Focus Input1
      </div>
      <div
        onClick={(e) => {
          refInput.current.blur();
        }}
      >
        Blur Input1
      </div>

      <button
        onClick={(e) => {
          ref1.current.style.backgroundColor = "red";
          ref1.current.style.color = "white";
        }}
      >
        {" "}
        change Bg of Babu
      </button>

      <button
        onClick={(e) => {
          ref2.current.style.backgroundColor = "green";
          ref2.current.style.color = "blue";
        }}
      >
        {" "}
        change Bg of Nani
      </button>

      <div ref={ref1}>Babu</div>
      <div ref={ref2}>Nani</div>

      <input ref={refInput}></input>
      <input></input>
    </div>
  );
};

export default Learn1UseRefHook;
