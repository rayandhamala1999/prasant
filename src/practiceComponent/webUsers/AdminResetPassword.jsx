import axios from "axios";
import React, { useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";

const AdminResetPassword = () => {
  let [password, setPassword] = useState("");
  let navigate = useNavigate();

  let [params] = useSearchParams();
  let token = params.get("token");

  let onSubmit = async (e) => {
    e.preventDefault();
    let data = {
      password: password,
    };
    try {
      let result = await axios({
        url: `http://localhost:8000/users/reset-password`,
        method: "PATCH",
        data: data,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      navigate("/admin/login");
    } catch (error) {
      toast.error(error.response.data.message);
      // console.log(error)
    }
  };

  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="password"> New Password:</label>
          <input
            type="password"
            placeholder="password should be strong"
            id="password"
            value={password}
            onChange={(e) => {
              // console.log("changed")
              // console.log(e.target.value)
              setPassword(e.target.value);
            }}
          ></input>
        </div>

        <button style={{ cursor: "pointer" }} type="submit">
          Reset
        </button>
      </form>
    </div>
  );
};
export default AdminResetPassword;


//read all user=>admin/super admin
//update user
//read specific user
// delete user