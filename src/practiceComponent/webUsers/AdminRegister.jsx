import axios from "axios";
import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";

const AdminRegister = () => {
  let [name, setName] = useState("");
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [dob, setDob] = useState("");
  let [gender, setGender] = useState("male");


  let data = {
    name: name,
    email: email,
    password: password,
    dob: dob,
    gender:gender
  };

  data={
    ...data,
    role:"admin"
  }

  let onSubmit =async (e) => {
    e.preventDefault();
  
  try {
    let result= await axios({
      url:`http://localhost:8000/users`,
      method:"POST",
      data:data

    })


    toast.success(" A link has been sent to your email.Please click the given link to verify your account.")
    setName("")
    setEmail("")
    setPassword("")
    setGender("")
    setDob("")
    

    
  } catch (error) {
    
    toast.error(error.response.data.message)
    // console.log(error)
  }  
}




  let genders = [
    {label:"Male", value:"male"},
    {label:"Female", value:"female"},
    {label:"Other", value:"other"},

  ]

  return (
    <div>
        <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="name">FullName:</label>
          <input
            type="text"
            placeholder="Eg:prasant dhamala"
            id="fullName"
            value={name}
            onChange={(e) => {
              // console.log("changed")
              // console.log(e.target.value)
              setName(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            placeholder="Eg:something@gmail.com"
            id="email"
            value={email}
            onChange={(e) => {
              // console.log("changed")
              // console.log(e.target.value)
              setEmail(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="password">Password:</label>
          <input
            type="password"
            placeholder="password should be strong"
            id="password"
            value={password}
            onChange={(e) => {
              // console.log("changed")
              // console.log(e.target.value)
              setPassword(e.target.value);
            }}
          ></input>
        </div>
      
        <div>
          <label htmlFor="dob">DateOfBirth:</label>
          <input
            type="date"
            placeholder="dateOfBirth"
            id="dob"
            value={dob}
            onChange={(e) => {
              // console.log("changed")
              // console.log(e.target.value)
              setDob(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label>Gender:</label>
          <br />
          {genders.map((item, i) => {
            return (
              <>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  value={item.value}
                  id={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </>
            );
          })}
        </div>

        <button type="submit">Proceed</button>
      </form>
    </div>
  );
        }
export default AdminRegister;
