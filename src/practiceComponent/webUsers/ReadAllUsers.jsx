import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const ReadAllUsers = () => {
  let [users, setUser] = useState([]);
  let navigate = useNavigate();

  let getAllUsers = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/users",
        method: "GET",
        headers:{
            Authorization:`Bearer ${localStorage.getItem("token")}`
        }
      });
      setUser(result.data.result)

    } catch (error) {
      console.log(error.message);
    }
  };

  let deleteUser = async (item) => {
    try {
      let result = await axios({
        url: `http://localhost:8000/users/${item._id}`,
        method: `delete`,
        headers:{
            Authorization:`Bearer ${localStorage.getItem("token")}`
        }
      });
      console.log(result);
      getAllUsers();
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getAllUsers();
  }, []);

  return (
    <div>
      {users.map((item, i) => {
        return (
          <div
            key={i}
            style={{ border: "solid red 3px", marginBottom: "20px" }}
          >
            <p>User: {item.name}</p>
            <p>Email: {item.email}</p>
            <p>Date of Birth:  {new Date(item.dob).toLocaleDateString()}</p>
            <p>Gender: {item.gender}</p>
            <button
              style={{ marginRight: "20px" }}
              onClick={() => {
                navigate(`/admin/${item._id}`);
              }}
            >
              View
            </button>

            <button
              style={{ marginRight: "20px" }}
              onClick={(e) => {
                navigate(`/admin/update/${item._id}`);
              }}
            >
              Edit
            </button>

            <button
              style={{ marginRight: "20px" }}
              onClick={() => {
                deleteUser(item);
              }}
            >
              Delete
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default ReadAllUsers;
