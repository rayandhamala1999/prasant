import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'

const ReadSpecificUser = () => {
   let navigate= useNavigate()
    let token =localStorage.getItem("token")
    let [user, setUser]=useState({})
    let params=useParams()
    let id=params.id
    let getReadSpecificUser=async()=>{
        try {
          let result= await axios({
            url:`http://localhost:8000/users/${id}`,
            method:"GET",
            headers:{
              "Authorization":`Bearer ${token}`
            }
          })
          console.log(result);
       setUser(result.data.result)
        } catch (error) {
          
        }
        
      }
      
    useEffect(()=>{
       getReadSpecificUser()
    },[])
  return (<div>
    
    <p>Full Name:{user.name}</p>
      <p>DateOfBirth: {new Date(user.dob).toLocaleDateString()}</p>
       <p>Email: {user.email}</p>
       
       <button onClick={()=>{
        navigate("/admin/user-update")
       }}>Update User</button>
 
  </div>)
}

export default ReadSpecificUser