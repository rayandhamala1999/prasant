import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";

const AdminUpdatePassword = () => {
  let [oldPassword, setOldPassword] = useState("");
  let [newPassword, setNewPassword] = useState("");
let navigate=useNavigate()


  let onSubmit = async (e) => {
    e.preventDefault();
    let data = {
      oldPassword: oldPassword,
      newPassword: newPassword,
    }
    try {
      let result = await axios({
        url: `http://localhost:8000/users/update-password`,
        method: "PATCH",
        data: data,
        headers:{
            "Authorization":`Bearer ${localStorage.getItem("token")}`
          }
      });
     
      localStorage.removeItem("token")
     navigate("/admin/login");
    } catch (error) {
      toast.error(error.response.data.message);
      // console.log(error)
    }
  };



  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="oldPassword"> Old Password:</label>
          <input
            type="password"
            placeholder="password should be strong"
            id="oldPassword"
            value={oldPassword}
            onChange={(e) => {
              // console.log("changed")
              // console.log(e.target.value)
              setOldPassword(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="newPassword"> New Password:</label>
          <input
            type="password"
            placeholder="password should be strong"
            id="newPassword"
            value={newPassword}
            onChange={(e) => {
              // console.log("changed")
              // console.log(e.target.value)
              setNewPassword(e.target.value);
            }}
          ></input>
        </div>

        <button type="submit">Update</button>
      </form>
    </div>
  );
};
export default AdminUpdatePassword;
