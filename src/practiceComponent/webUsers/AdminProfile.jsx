import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

const AdminProfile = () => {
   let navigate= useNavigate()
    let token =localStorage.getItem("token")
    let [profile, setProfile]=useState({})
    let getAdminProfile=async()=>{
        try {
          let result= await axios({
            url:"http://localhost:8000/users/my-profile",
            method:"GET",
            headers:{
              "Authorization":`Bearer ${token}`
            }
          })
          console.log(result);
       setProfile(result.data.result)
        } catch (error) {
          
        }
        
      }
      
    useEffect(()=>{
       getAdminProfile()
    },[])
  return (<div>
    
    <p>Full Name:{profile.name}</p>
      <p>DateOfBirth: {new Date(profile.dob).toLocaleDateString()}</p>
       <p>Email: {profile.email}</p>
       
       <button onClick={()=>{
        navigate("/admin/profile-update")
       }}>UpdateProfile</button>
 
  </div>)
}

export default AdminProfile