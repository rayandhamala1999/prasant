import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";

const UpdateSpecificUser = () => {
  let navigate = useNavigate();
  let [name, setName] = useState("");
  let [dob, setDob] = useState("");
  let [gender, setGender] = useState("male");
  let token = localStorage.getItem("token");

  let params = useParams();
  let id = params.id;

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      name: name,
      dob: dob,
      gender: gender,
    };

    try {
      let result = await axios({
        url: `http://localhost:8000/users/${id}`,
        method: "PATCH",
        data: data,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      navigate(`/admin/${id}`);
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  let getAdminUser = async () => {
    try {
      let result = axios({
        url: `http://localhost:8000/users/${id}`,
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      let data = result.data.data;
      setDob(data.dob);
      setName(data.name);
      setGender(data.gender);
    } catch (error) {}
  };

  useEffect(() => {
    getAdminUser();
  }, []);

  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="name">FullName:</label>
          <input
            type="text"
            placeholder="Eg:prasant dhamala"
            id="fullName"
            value={name}
            onChange={(e) => {
              // console.log("changed")
              // console.log(e.target.value)
              setName(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="dob">DateOfBirth:</label>
          <input
            type="date"
            placeholder="dateOfBirth"
            id="dob"
            value={dob}
            onChange={(e) => {
              // console.log("changed")
              // console.log(e.target.value)
              setDob(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label>Gender:</label>
          <br />
          {genders.map((item, i) => {
            return (
              <>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  value={item.value}
                  id={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </>
            );
          })}
        </div>

        <button type="submit">Proceed</button>
      </form>
    </div>
  );
};
export default UpdateSpecificUser;
