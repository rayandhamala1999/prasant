import React from 'react'

const Info = ({name,age,fatherDetails,favFood})=> {
  return (
    <div>name is {name}<br></br>
    age is {age}<br></br>
    fatherName is {fatherDetails.name}<br></br>
    favFood is {favFood}
    </div>
  )
}

export default Info