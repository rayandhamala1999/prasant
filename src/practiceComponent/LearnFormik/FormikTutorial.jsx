import { Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "./FormikInput";
import FormikRadio from "./FormikRadio";
import FormikTextArea from "./FormikTextArea";
import FormikSelect from "./FormikSelect";
import FormikCheckbox from "./FormikCheckbox";

const FormikTutorial = () => {
  let initialValues = {
    fullName: "",
    email: "",
    password: "",
    gender:  "male",
    country: "Nepal",
    isMarried: false,
    description: "",
    phoneNumber:"",
    age: 0,
  };
  let onSubmit = (value, other) => {
    console.log(value);
  };
  let validationSchema = yup.object({
    fullName: yup.string().required("Name is required.").min(5,"must be at least 5 characters.")
    .max(15,"must be at least at most 15 characters"),
    
    email: yup.string().required("Email is required.").matches(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,"Email is not valid!"),
    password: yup.string().required("Password is required.").matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,"Password should have minimum eight characters, at least one letter and one number:"
    ),
    gender: yup.string().required("Gender is required."),
    country: yup.string().required("Country is required."),
    isMarried: yup.boolean(),
    description: yup.string(),
    phoneNumber: yup.string().required("PhoneNumber is required.").min(10,"Only 10 characters are allowed").max(10,"should have 10 characters").matches(/^[0-9]*$/,"Enter the valid number!"),
    age: yup.number().required("Age is required.").min(18,"Age must be more than 18"),
  });
  let genderOptions = [
    {
      label: "Male",
      value: "male",
    },
    {
      label: "Female",
      value: "female",
    },
    {
      label: "Other",
      value: "other",
    },
  ];

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="fullName"
                label="Full Name:"
                type="text"
                onChange={(e) => {
                  formik.setFieldValue("fullName", e.target.value);
                }}
                placeholder="eg:Prasant Dhamala"
                required={true}
              ></FormikInput>

              <FormikInput
                name="email"
                label="Email:"
                type="email"
                onChange={(e) => {
                  formik.setFieldValue("email", e.target.value);
                }}
                placeholder="eg:something@gmail.com"
                required={true}
              ></FormikInput>

              <FormikInput
                name="password"
                label="Password:"
                type="password"
                onChange={(e) => {
                  formik.setFieldValue("password", e.target.value);
                }}
                placeholder="***********"
                required={true}
              ></FormikInput>

              <FormikTextArea
                name="description"
                label={"Description:"}
                type={"text"}
                placeholder="write something here!"
              ></FormikTextArea>

              <FormikSelect
                name="country"
                label={"Country:"}
                onChange={(e) => {
                  formik.setFieldValue("country", e.target.value);
                }}
                required={true}
                options={[
                  { label: "Select Country", value: "", disabled: true },
                  {
                    label: "Nepal",
                    value: "nep",
                  },
                  {
                    label: "India",
                    value: "ind",
                  },
                  {
                    label: "China",
                    value: "chi",
                  },
                  {
                    label: "America",
                    value: "ame",
                  },
                ]}
              ></FormikSelect>

              <FormikRadio
                name="gender"
                label="Gender:"
                onChange={(e) => {
                  formik.setFieldValue("gender",e.target.value);
                }}
                required={true}
                options={genderOptions}
              ></FormikRadio>

              <FormikCheckbox
                name="isMarried"
                label="Is Married"
                onChange={(e) => {
                  formik.setFieldValue("isMarried", e.target.checked);
                }}
              ></FormikCheckbox>

              <FormikInput
                name="age"
                label="Age:"
                type="number"
                onChange={(e) => {
                  formik.setFieldValue("age", e.target.value);
                }}
                required={true}
              ></FormikInput>

              <FormikInput
                name="phoneNumber"
                label="Phone Number:"
                type="text"
                onChange={(e) => {
                  formik.setFieldValue("Pnumber", e.target.value);
                }}
                required={true}
              ></FormikInput>

              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default FormikTutorial;
