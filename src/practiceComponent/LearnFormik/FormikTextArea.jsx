import { Field, Form, Formik } from "formik";
import React from "react";

const FormikTextArea = ({ name, label, type,required, onChange, ...props }) => {
  return (
    <div>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <div>
              <label htmlFor={name}>
                {label}{" "}
                {required?<span style={{ color: "red" }}>*</span>:null}
              </label>
              <textarea
                {...field}
                {...props}
                type={type}
                id={name}
                value={meta.value}
                onChange={onchange ? onChange : field.onChange}
                // onChange={(e) => {
                //     formik.setFieldValue("name", e.target.value);
                // }}
              ></textarea>
              {meta.touched && meta.error ? (
                <div style={{ color: "red" }}>{meta.error}</div>
              ) : null}
            </div>
          );
        }}
      </Field>
    </div>
  );
};

export default FormikTextArea;

// generally !!-gives true or false value because it convert into boolean thing
//props gives the remaining task and operators 