import { Field, Form, Formik } from "formik";
import React from "react";

const FormikSelect = ({
  name,
  label,
  type,
  required,
  onChange,
  options,
  ...props
}) => {
  return (
    <div>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <di>
              <label htmlFor={name}>
                {label}{" "}
                {required ? <span style={{ color: "red" }}>*</span> : null}
              </label>
              <select
                {...field}
                {...props}
                id={name}
                value={meta.value}
                onChange={onChange ? onChange : field.onChange}
                required={true}
              >
                {options.map((item, i) => {
                  return (
                    <option key={i} value={item.value} disabled={item.disabled}>
                      {item.label}
                    </option>
                  );
                })}
              </select>
              {meta.touched && meta.error ? (
                <div style={{ color: "red" }}>{meta.error}</div>
              ) : null}
            </di>
          );
        }}
      </Field>
    </div>
  );
};

export default FormikSelect;

// generally !!-gives true or false value because it convert into boolean thing
//props gives the remaining task and operators
