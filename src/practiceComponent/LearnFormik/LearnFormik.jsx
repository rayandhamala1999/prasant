import { Field, Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "./FormikInput";
import FormikTextArea from "./FormikTextArea";
import FormikSelect from "./FormikSelect";
import FormikRadio from "./FormikRadio";
import FormikCheckbox from "./FormikCheckbox";

const LearnFormik = () => {
  let initialValues = {
    name: "",
    sureName: "",
    description: "",
    country: "",
    gender: "male",
    isMarried: false,
  };

  let onSubmit = (value, other) => {
    console.log(value);
  };

  let validationSchema = yup.object({
    name: yup.string().required("Name is required."),
    sureName: yup.string().required("SureName is required."),
    description: yup.string().required("Description is required."),
  });

  let genderOption = [
    {
      label: "Male",
      value: "male",
    },
    {
      label: "Female",
      value: "female",
    },
    {
      label: "Other",
      value: "other",
    },
  ];

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <Form>
              {/* <Field name="name">
                {({ field, form, meta }) => {
                  return (
                    <div>
                      <label htmlFor="name">Name:</label>
                      <input
                      {...field}
                        type="text"
                        placeholder="Eg:prasant"
                        id="name"
                        value={meta.value}
                        onChange={(e) => {
                          formik.setFieldValue("name", e.target.value);
                        }}
                      ></input>
                      {meta.touched&&meta.error ? (
                        <div style={{ color: "red" }}>{meta.error}</div>
                      ) : null}
                     
                    </div>
                  );
                }}
              </Field> */}

              <FormikInput
                name="name"
                label={"Name:"}
                onChange={(e) => {
                  formik.setFieldValue("name", e.target.value);
                }}
                type="text"
                placeholder="eg:Prasant"
                required={true}
              ></FormikInput>

              <FormikInput
                name="sureName"
                label={"SureName:"}
                type={"text"}
                placeholder="eg:Dhamala"
                required={true}
              ></FormikInput>
              <FormikTextArea
                name="description"
                label={"Description:"}
                type={"text"}
                placeholder="write something here!"
              ></FormikTextArea>
              <FormikSelect
                name="country"
                label={"Country:"}
                onChange={(e) => {
                  formik.setFieldValue("country", e.target.value);
                }}
                required={true}
                options={[
                  { label: "Select Country", value: "", disabled: true },
                  {
                    label: "Nepal",
                    value: "nep",
                  },
                  {
                    label: "India",
                    value: "ind",
                  },
                  {
                    label: "China",
                    value: "chi",
                  },
                  {
                    label: "America",
                    value: "ame",
                  },
                ]}
              ></FormikSelect>

              <FormikRadio
                name="gender"
                label="Gender:"
                onChange={(e) => {
                  formik.setFieldValue("gender", e.target.value);
                }}
                required={true}
                options={genderOption}
              ></FormikRadio>
              
              <FormikCheckbox
                name="isMarried"
                label="Is Married"
                onChange={(e) => {
                  formik.setFieldValue("isMarried", e.target.checked);
                }}
              ></FormikCheckbox>

              {/*                
              <Field name="sureName">
                {({ field, form, meta }) => {
                  return (
                    <div>
                      <label htmlFor="sureName">SureName:</label>
                      <input
                      {...field}
                        type="text"
                        placeholder="Eg:Dhamala"
                        id="sureName"
                        value={meta.value}
                        onChange={(e) => {
                          formik.setFieldValue("sureName", e.target.value);
                        }}
                      ></input>
                      {meta.touched&&meta.error ? (
                        <div style={{ color: "red" }}>{meta.error}</div>
                      ) : null}
                    </div>
                  );
                }}
              </Field> */}

              {/* <Field name="description">
                {({ field, form, meta }) => {
                  return (
                    <div>
                      <label htmlFor="description">Description:</label>
                      <input
                        {...field}
                        type="text"
                        placeholder="describe something"
                        id="description"
                        value={meta.value}
                        // onChange={field.onChange}
                        onChange={(e) => {
                          formik.setFieldValue("description", e.target.value);
                        }}
                        //  when we have to multiple task then we can do this code
                      ></input>
                      {meta.touched && meta.error ? (
                        <div style={{ color: "red" }}>{meta.error}</div>
                      ) : null}
                    </div>
                  );
                }}
              </Field> */}

              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default LearnFormik;

// each field has three things
//value
//error
//touch

// formik is for state management
// and for the validation in react we use yup

//validation will only run only if
//onChange is fire
//onBlur(touched) event is fire
//onSubmit event is fire
