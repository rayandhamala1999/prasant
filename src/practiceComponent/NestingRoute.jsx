import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import AdminRegister from "./webUsers/AdminRegister";

const NestingRoute = () => {
  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              hello prasant <Outlet></Outlet> //outlet is used for the parents
              and it gives its child to run
            </div>
          }
        >
          <Route
            path="student"
            element={
              <div>
                {" "}
                hello students<Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<div> student page</div>}></Route>
            <Route path="1" element={<div>1</div>}>
              {" "}
            </Route>
            <Route path="rayan" element={<div>this is rayan page</div>}></Route>
            <Route path="*" element={<div>404 page not found </div>}></Route>
          </Route>

          <Route index element={<div>home page</div>}></Route>
          <Route path="*" element={<div>404 page not found </div>}></Route>
        </Route>
        
      </Routes>
    </div>
  );
};

export default NestingRoute;

// /products => read all products
// /products/:id => detail page
// /products/create => create products
// /products/update/:Id => products update
