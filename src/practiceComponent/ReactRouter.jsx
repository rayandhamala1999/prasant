import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import MyLinks from "./MyLinks";
import ReadAllProduct from "./product/ReadAllProduct";
import ReadSpecificProduct from "./product/ReadSpecificProduct";
import CreateProduct from "./product/CreateProduct";
import UpdateProduct from "./product/UpdateProduct";
import ReadAllStudents from "./student/ReadAllStudents";
import ReadSpecificStudents from "./student/ReadSpecificStudents";
import CreateStudents from "./student/CreateStudents";
import UpdateStudent from "./student/UpdateStudent";
import AdminRegister from "./webUsers/AdminRegister";
import AdminVerify from "./webUsers/AdminVerify";
import AdminLogin from "./webUsers/AdminLogin";
import AdminProfile from "./webUsers/AdminProfile";
import AdminLogout from "./webUsers/AdminLogout";
import AdminProfileUpdate from "./webUsers/AdminProfileUpdate";
import AdminUpdatePassword from "./webUsers/AdminUpdatePassword";
import AdminForgotPassword from "./webUsers/AdminForgotPassword";
import AdminResetPassword from "./webUsers/AdminResetPassword";
import ReadAllUsers from "./webUsers/ReadAllUsers";
import ReadSpecificUser from "./webUsers/ReadSpecificUser";
import UpdateSpecificUser from "./webUsers/UpdateSpecificUser";

const ReactRouter = () => {
  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <MyLinks></MyLinks>
              <Outlet></Outlet>
              {/* <div>This is Footer</div> */}
            </div>
          }
        >
          <Route index element={<div>Home Page</div>}></Route>

          <Route
            path="products"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllProduct></ReadAllProduct>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificProduct></ReadSpecificProduct>}
            ></Route>
            <Route
              path="create"
              element={<CreateProduct></CreateProduct>}
            ></Route>

            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateProduct></UpdateProduct>}
              ></Route>
            </Route>
          </Route>

          <Route
            path="students"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllStudents></ReadAllStudents>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificStudents></ReadSpecificStudents>}
            ></Route>
            <Route
              path="create"
              element={<CreateStudents></CreateStudents>}
            ></Route>

            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateStudent></UpdateStudent>}
              ></Route>
            </Route>
          </Route>
          <Route path="reset-password" element={<AdminResetPassword></AdminResetPassword>}></Route>
          <Route
            path="admin"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
           
          >
           <Route path="my-profile" element={<AdminProfile></AdminProfile>}></Route>
           <Route path="logout" element={<AdminLogout></AdminLogout>}></Route>
           <Route path="profile-update" element={<AdminProfileUpdate></AdminProfileUpdate>}></Route>
           <Route path="update-password" element={<AdminUpdatePassword></AdminUpdatePassword>}></Route>
           <Route path="forgot-password" element={<AdminForgotPassword></AdminForgotPassword>}></Route>
           <Route path="read-all-user" element={<ReadAllUsers></ReadAllUsers>}></Route>
           <Route path=":id" element={<ReadSpecificUser></ReadSpecificUser>}></Route>
           <Route path="update" element={<div><Outlet></Outlet></div>}
           > 
           <Route path=":id" element={<UpdateSpecificUser></UpdateSpecificUser>}></Route>
           </Route>

            <Route
              index
              element={<div>This is the admin dashboard</div>}
            ></Route>
            <Route
              path="register"
              element={<AdminRegister></AdminRegister>}
            ></Route>
            <Route path="login" element={<AdminLogin></AdminLogin>}></Route>
          </Route>
          <Route
          path="verify-email"
          element={<AdminVerify></AdminVerify>}
        ></Route>
        </Route>
       
      </Routes>
    </div>
  );
};

export default ReactRouter;
