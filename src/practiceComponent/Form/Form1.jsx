import React, { useState } from 'react'

const Form1 = () => {

    let[name,setName]=useState("")
    let[sureName,setSureName]=useState("")
    let[email,setEmail]=useState("")
    let[password,setPassword]=useState("")
    let [phoneNumber,setPhoneNumber]=useState("")
    let [dob,setDob]=useState("")
    let [description,setDescription]=useState("")

    let data={
        name:name,
        sureName:sureName,
        email:email,
        password:password,
        phoneNumber:phoneNumber,
        dob:dob,
        description:description
    }

    let onSubmit=(e)=>{
        e.preventDefault()
        console.log(data)
        
    }

  
  return (
    <div>
        <form onSubmit={onSubmit}>
            <div>
                <label htmlFor='name'>Name:</label>
                <input type="text" 
                placeholder='Eg:prasant'
                id="name"
                value={name}
                onChange={(e)=>{
                    // console.log("changed")
                    // console.log(e.target.value)
                    setName(e.target.value)
                }}

                ></input>
            </div>

            <div>
                <label htmlFor='surename'>SureName:</label>
                <input type="text" 
                placeholder='Eg:Dhamala'
                id="surename"
                value={sureName}
                onChange={(e)=>{
                    // console.log("changed")
                    // console.log(e.target.value)
                    setSureName(e.target.value)
                }}

                ></input>
            </div>
            <div>
                <label htmlFor='email'>Email:</label>
                <input type="email" 
                placeholder='Eg:something@gmail.com'
                id="email"
                value={email}
                onChange={(e)=>{
                    // console.log("changed")
                    // console.log(e.target.value)
                    setEmail(e.target.value)
                }}

                ></input>
            </div>
            <div>
                <label htmlFor='password'>Password:</label>
                <input type="password" 
                placeholder='password should be strong'
                id="password"
                value={password}
                onChange={(e)=>{
                    // console.log("changed")
                    // console.log(e.target.value)
                    setPassword(e.target.value)
                }}

                ></input>
            </div>
            <div>
                <label htmlFor='phoneNumber'>PhoneNumber:</label>
                <input type="number" 
                placeholder='number'
                id="phoneNumber"
                value={phoneNumber}
                onChange={(e)=>{
                    // console.log("changed")
                    // console.log(e.target.value)
                    setPhoneNumber(e.target.value)
                }}
                ></input>
            </div>
            <div>
                <label htmlFor='dob'>DateOfBirth:</label>
                <input type="date" 
                placeholder='dateOfBirth'
                id="dob"
                value={dob}
                onChange={(e)=>{
                    // console.log("changed")
                    // console.log(e.target.value)
                    setDob(e.target.value)
                }}

                ></input>
            </div>
            <div>
            <label htmlFor='description'>Description:</label>
            <textarea placeholder='description'
            id='description'
            value={description}
            onChange={(e)=>{
                setDescription(e.target.value)
            }}></textarea>
            </div>
            
            <button type="submit">
                Proceed 
            </button>
        </form>
    </div>
  )
}

export default Form1