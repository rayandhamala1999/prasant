import React, { useState } from 'react';
const GenderSelection = () => {
  let [label, setLabel] = useState('male');
  let [value, setValue] = useState('male');
  let onSubmit = (e) => {
    e.preventDefault();
    let data = {
      label: label,
      value: value,
    };
    console.log(data);
  };
  let genders = [
    { label: 'Male', value: 'male' },
    { label: 'Female', value: 'female' },
    { label: 'Other', value: 'other' },
  ];
  return (
    <div>
      <form onSubmit={onSubmit}>
        <div>
          <label>Gender:</label>
          <select
            value={value}
            onChange={(e) => {
              setValue(e.target.value);
              setLabel(
                genders.find((gender) => gender.value === e.target.value).label
              );
            }}
          >
            {genders.map((gender, i) => (
              <option key={i} value={gender.value}>
                {gender.label}
              </option>
            ))}
          </select>
        </div>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};
export default GenderSelection;