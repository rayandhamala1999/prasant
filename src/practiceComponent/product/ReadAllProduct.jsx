import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const ReadAllProduct = () => {
  let [products, setProduct] = useState([]);
  let navigate = useNavigate();

  let getAllProducts = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/products",
        method: "GET",
      });
      setProduct(result.data.result);
    } catch (error) {
      console.log(error.message);
    }
  };

  let deleteProduct = async (item) => {
    try {
      let result = await axios({
        url: `http://localhost:8000/products/${item._id}`,
        method: `delete`,
      });
      console.log(result);
      getAllProducts();
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getAllProducts();
  }, []);

  return (
    <div>
      {products.map((item, i) => {
        return (
          <div
            key={i}
            style={{ border: "solid red 3px", marginBottom: "20px" }}
          >
            <p>product is {item.name}</p>
            <p>product price NRs. {item.price}</p>
            <p>product quantity {item.quantity}</p>
            <button
              style={{ marginRight: "20px" }}
              onClick={() => {
                navigate(`/products/${item._id}`);
              }}
            >
              View
            </button>

            <button
              style={{ marginRight: "20px" }}
              onClick={(e) => {
                navigate(`/products/update/${item._id}`);
              }}
            >
              Edit
            </button>

            <button
              style={{ marginRight: "20px" }}
              onClick={() => {
                deleteProduct(item);
              }}
            >
              Delete
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default ReadAllProduct;
