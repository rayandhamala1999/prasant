import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const UpdateProduct = () => {
let[name,setName]=useState("")
let[price,setPrice]=useState("")
let[quantity,setQuantity]=useState("")

let getProduct = async () => {
  let result = await axios({
    url:`http://localhost:8000/products/${params.id}`,
    method:"GET"
  })
  let data = result.data.result
  setName(data.name)
  setPrice(data.price)
  setQuantity(data.quantity)
  // setProduct(result.data.result)
};


useEffect(() => {
  getProduct()
},[])


let params=useParams()
let onSubmit=async (e)=>{
  e.preventDefault()
  // console.log(data)

  let data={
    name:name,
  price:price,
  quantity:quantity
  }
  
  try {
    let result=await axios({
      url:`http://localhost:8000/products/${params.id}`,
      method:"PATCH",
      data:data

    })

  //  console.log(result)

    setName("")
    setPrice("")
    setQuantity("")
    toast.success(result.data.message, {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
      });

    
  } catch (error) {
    
    toast.error(error.response.data.message, {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
      });
    // console.log(error)
  }  
}

  return (
    <div>
      <ToastContainer />
<form onSubmit={onSubmit}>
   <div>
                <label htmlFor='name'>Name:</label>
                <input type="text" 
                placeholder='Eg:potato'
                id="name"
                value={name}
                onChange={(e)=>{
                    setName(e.target.value)
                }}

                ></input>
          
          <label htmlFor='price'>Price:</label>
                <input type="Number" 
                placeholder='Rs:200'
                id="price"
                value={price}
                onChange={(e)=>{
                    setPrice(e.target.value)
                }}
                ></input>


          <label htmlFor='quantity'>Quantity:</label>
                <input type="Number" 
                placeholder='Eg:10kg'
                id="quantity"
                value={quantity}
                onChange={(e)=>{
                    setQuantity(e.target.value)
                }}
                ></input>

   </div>
   <button type="submit">
                Update
            </button>
</form>



    </div>
  )
}

export default UpdateProduct