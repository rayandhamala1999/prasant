import React, { useState } from 'react'

const Name = () => {
    let [isVisible,setName]=useState(true) 

    let handleName=(isVisible)=>{
      return(e)=>{
          setName(isVisible)
      }
  }
                                         
  return (
    <div>
 {isVisible?<div>Prasant Dhamala</div>:null}
<button onClick={handleName(true)}>Show</button> <br></br>
<button onClick={handleName(false)}>Hide</button> <br></br>
    </div>
  )
}

export default Name