import React, { useState } from 'react'

const IncrementByChoice = () => {
  
let [count,setCount]=useState(0) 
    
                                                  
    let handleIncrement=(e)=>{
       if(count<10){ 
      setCount(count+2)
       }
    }

    let handleIncrementBy100=(e)=>{
        if(count<1000){ 
       setCount(count+100)
        }
     }
    let handleIncrementBy1000=(e)=>{
        if(count<10000){ 
       setCount(count+1000)
        }
     }
   let decrement=(e)=>{
       if(count>0){
       setCount(count-1)}
   }
    let reset=(e)=>{
       setCount(0)
    }
 return (
   <div>
count is  {count}
<button onClick={handleIncrement}>incrementedBy2</button> <br></br>
<button onClick={handleIncrementBy100}>incrementedBy100</button> <br></br>
<button onClick={handleIncrementBy1000}>incrementedBy1000</button> <br></br>
<button onClick={decrement}>decrement</button> <br></br>
<button onClick={reset}>reset</button> <br></br>
   </div>
 )


 }

export default IncrementByChoice