import React from 'react'
import { NavLink } from 'react-router-dom'

const MyLinks = () => {
  return (
    <div>

        <NavLink to="/products/create" style={{marginRight:"20px"}}> Create Product</NavLink>
        <NavLink to="/products" style={{marginRight:"20px"}}>Product</NavLink>
        <NavLink to="/students/create" style={{marginRight:"20px"}}>Create student</NavLink>
        <NavLink to="/students" style={{marginRight:"20px"}}>Student</NavLink>
        <NavLink to="admin/register" style={{marginRight:"20px"}}>Admin Register</NavLink> 
        <NavLink to="admin/login" style={{marginRight:"20px"}}>Admin login </NavLink>
        <NavLink to="admin/my-profile" style={{marginRight:"20px"}}>My Profile </NavLink>
        <NavLink to="admin/logout" style={{marginRight:"20px"}}>Logout </NavLink>
        <NavLink to="admin/update-password" style={{marginRight:"20px"}}>UpdatePassword </NavLink>
        <NavLink to="admin/read-all-user" style={{marginRight:"20px"}}>ReadAllUsers</NavLink>
    </div>
  )
}

export default MyLinks