import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import CreateProduct from "./product/CreateProduct";
import ReadAllProduct from "./product/ReadAllProduct";
import CreateStudents from "./student/CreateStudents";
import ReadAllStudents from "./student/ReadAllStudents";
import ReadSpecificProduct from "./product/ReadSpecificProduct";
import ReadSpecificStudents from "./student/ReadSpecificStudents";
import AdminRegister from "./webUsers/AdminRegister";

const MyRoutes = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<div>This is the home page </div>}></Route>
        <Route
          path="/products/create"
          element={<CreateProduct></CreateProduct>}
        ></Route>
        <Route
          path="/products"
          element={<ReadAllProduct></ReadAllProduct>}
        ></Route>
        <Route
          path="/products/:id"
          element={<ReadSpecificProduct></ReadSpecificProduct>}
        >
          {" "}
        </Route>

        <Route
          path="/students/create"
          element={<CreateStudents></CreateStudents>}
        ></Route>
        <Route
          path="/students"
          element={<ReadAllStudents></ReadAllStudents>}
        ></Route>
        <Route
          path="/students/:id"
          element={<ReadSpecificStudents></ReadSpecificStudents>}
        >
          {" "}
        </Route>
        <Route path="/*" element={<div>404 not found</div>}></Route>

        <Route path="/*" element={<div>404 not found</div>}></Route>
      </Routes>
    </div>
  );
};

export default MyRoutes;
