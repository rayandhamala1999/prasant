import React from "react";

const RemoveLocalStorageData = () => {
  return (
    <div>
      <button
        onClick={() => {
          localStorage.removeItem("token");
          localStorage.removeItem("name");
          localStorage.removeItem("age");
          localStorage.removeItem("isMarried");
        }}
      >
        Remove
      </button>

      <button></button>
    </div>
  );
};

export default RemoveLocalStorageData;

// Local storage is the browser's memory for a particular URL.
// the data in a local storage persist even when the session ends (browser close)(tab close)
