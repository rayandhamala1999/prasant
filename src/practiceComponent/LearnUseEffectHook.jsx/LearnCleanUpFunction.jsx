import React, { useEffect, useState } from 'react'

const LearnCleanUpFunction = () => {
let[count,setCount]=useState(0)

useEffect(()=>{
    console.log("i am useEffect")
    return()=>{
        console.log(" i am cleanUpFunction ")
    }
},[count])


//clean up function are those functions which is return by useEffect
//clean up function doesn't execute in first render 
//but from the second render the clean up function will execute if useEffect fun run 

// what happens when useEffect gets execute 
//first cleanUp function will run then the code above it will execute 
//component mount and Unmount 
//when component is unmount nothing gets executed except cleanup function 

  return (
    <div>
     count is  {count}
     <br></br>
     <button onClick={()=>{setCount(count+1)}}>Increment Count1 </button>
    </div>
  )
}

export default LearnCleanUpFunction