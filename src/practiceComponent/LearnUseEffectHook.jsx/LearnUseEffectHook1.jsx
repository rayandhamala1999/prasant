import React, { useEffect } from 'react'

const LearnUseEffectHook1 = () => {
    useEffect(()=>{
        console.log("i am useEffect function")
    },[])
    console.log("*******")
  return (
    <div>LearnUseEffectHook1</div>
  )
}

export default LearnUseEffectHook1