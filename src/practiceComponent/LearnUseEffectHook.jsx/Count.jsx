import React, { useEffect, useState } from 'react'

const Count = () => {
    let[count,setCount]=useState(0)
    let[count2,setCount2]=useState(100)

    useEffect(()=>{
        console.log("i am UseEffect FUnction")
    },[count,count2])
//it execute for the first render
//

    useEffect(()=>{ 
      console.log("i am useEffect function2")
    },[count])//it execute for the 1st render
    //from the second render the function will only execute if the count variable is changed 
    //from the second render the function will only execute if the count and count2 variable is changed
    useEffect(()=>{ 
      console.log("i am useEffect function3")
    },[])//it execute for the first render only

    useEffect(()=>{ // it is similar with console.log("useEffect")
     console.log("i am useEffect function4 ")
    })//it execute for each render 
    
  return (

    <div>
         count is {count}
        <button onClick={(e)=>{
            setCount(count+1)
        }}>Increment</button>
        <br></br>
        count2 is {count2}
        <button onClick={(e)=>{
            setCount2(count2+1)
        }}>IncrementCount2</button>
    </div>
  )
}

export default Count