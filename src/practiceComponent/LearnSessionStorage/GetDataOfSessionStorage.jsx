import React from 'react'

const GetDataOfSessionStorage = () => {
    console.log(sessionStorage.getItem("token"))
    console.log(sessionStorage.getItem("name"))

    //to remove the item form the session 
          sessionStorage.removeItem("name")
    
  return (
    <div>GetDataOfSessionStorage</div>
  )
}

export default GetDataOfSessionStorage

//session storage is the browser memory of the particular url in the particular tab (session)
//data will persist in the given session(ie data will removed when the tab closed )