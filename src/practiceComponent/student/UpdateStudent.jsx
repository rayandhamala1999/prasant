import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const UpdateStudent = () => {
let[name,setName]=useState("")
let[age,setAge]=useState("")
let[isMarried,setIsMarried]=useState("")

let getStudent = async () => {
  let result = await axios({
    url:`http://localhost:8000/students/${params.id}`,
    method:"GET"
  })
  let data = result.data.result
  setName(data.name)
  setAge(data.age)
  setIsMarried(data.isMarried)
  // setProduct(result.data.result)
};


useEffect(() => {
  getStudent()
},[])


let params=useParams()
let onSubmit=async (e)=>{
  e.preventDefault()
  // console.log(data)

  let data={
    name:name,
  age:age,
  isMarried:isMarried
  }
  
  try {
    let result=await axios({
      url:`http://localhost:8000/students/${params.id}`,
      method:"PATCH",
      data:data

    })

  //  console.log(result)

    setName("")
    setAge("")
    setIsMarried("")
    toast.success(result.data.message, {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
      });

    
  } catch (error) {
    
    toast.error(error.response.data.message, {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
      });
    // console.log(error)
  }  
}

  return (
    <div>
      <ToastContainer />
<form onSubmit={onSubmit}>
   <div>
                <label htmlFor='name'>Name:</label>
                <input type="text" 
                placeholder='Eg:potato'
                id="name"
                value={name}
                onChange={(e)=>{
                    setName(e.target.value)
                }}

                ></input>
          
          <label htmlFor='age'>Age:</label>
                <input type="Number" 
                placeholder='eg:20yrs'
                id="age"
                value={age}
                onChange={(e)=>{
                    setAge(e.target.value)
                }}
                ></input>

<label htmlFor='isMarried'>IsMarried:</label>
                <input 
                type="checkbox" 
                id="isMarried"
                // value={isMarried}
                checked={isMarried===true}
                onChange={(e)=>{
                    // console.log("changed")
                    // console.log(e.target.value)
                    setIsMarried(e.target.checked)
                  
                }}

                ></input>
          

   </div>
   <button type="submit">
                Update
            </button>
</form>
    </div>
  )
}

export default UpdateStudent