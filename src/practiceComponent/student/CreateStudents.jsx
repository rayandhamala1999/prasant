import axios from 'axios'
import React, { useState } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const CreateStudents = () => {
  let[name,setName]=useState("")
  let[age,setAge]=useState("")
  let[isMarried,setIsMarried,]=useState("")
  
  
  let onSubmit=async (e)=>{
    e.preventDefault()
    // console.log(data)
  
    let data={
      name:name,
    age:age,
    isMarried:isMarried
    }
    
    try {
      let result=await axios({
        url:`http://localhost:8000/students`,
        method:"POST",
        data:data
  
      })
  
    //  console.log(result)
      setName("")
      setAge("")
      setIsMarried(false)
      toast.success(result.data.message, {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        });
  
      
    } catch (error) {
      
      toast.error("Unable to create product!", {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        });
      // console.log(error)
    }  
  }
  
    return (
      <div>
        <ToastContainer />
  <form onSubmit={onSubmit}>
     <div>
                  <label htmlFor='name'>Name:</label>
                  <input type="text" 
                  placeholder='Eg:Prasant Dhamala'
                  id="name"
                  value={name}
                  onChange={(e)=>{
                      setName(e.target.value)
                  }}
  
                  ></input>
            
            <label htmlFor='age'>Age:</label>
                  <input type="Number" 
                  placeholder='20'
                  id="age"
                  value={age}
                  onChange={(e)=>{
                      setAge(e.target.value)
                  }}
                  ></input>
            
  
  
           <div>

                <label htmlFor='isMarried'>IsMarried</label>
                <input 
                type="checkbox" 
                id="isMarried"
                // value={isMarried}
                checked={isMarried===true}
                onChange={(e)=>{
                    // console.log("changed")
                    // console.log(e.target.value)
                    setIsMarried(e.target.checked)
                  
                }}

                ></input>
            </div>
  
     </div>
     <button type="submit">
                  Proceed 
              </button>
  </form>
  
  
  
      </div>
    )
}

export default CreateStudents