import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

const ReadAllStudents = () => {

  let [students,setStudents]=useState([])
  let navigate=useNavigate()

  let getAllStudents=async()=>{
    let result=await axios({
      url:"http://localhost:8000/students",
      method:"GET"
    })
    setStudents(result.data.result)
  }

  let deleteStudents = async (item) => {
    try {
            let result = await axios({
              url: `http://localhost:8000/students/${item._id}`,
              method: `delete`
            });
            console.log(result);
            getAllStudents();
    } catch (error) {
      console.log(error.message);
    }
  }

  
  useEffect(()=>{
    getAllStudents()
  },[])


return (
  <div>
{
  students.map((item,i)=>{
    return(
      <div key={i}
      style={{border:"solid green 3px",marginBottom:"20px"}}>
        <p>Student name is {item.name}</p>
        <p>Student age is {item.age}</p>
        <p>IsMarried :{item.isMarried?<div>Married</div>:<div>UmMarried</div>}</p>

      <button style={{marginRight:"20px"}} onClick={()=>{
        navigate (`/students/${item._id}`)
      }}>View</button>
      
        <button style={{marginRight:"20px"}} onClick={(e)=>{
          navigate(`/students/update/${item._id}`)}}>Edit</button>

        <button style={{marginRight:"20px"}} onClick={()=>{
            deleteStudents(item);
        }}>Delete</button>
    </div>
    )
  })
}
  </div>
)

}
export default ReadAllStudents