import './App.css';
import Address from './practiceComponent/Address';
import Age from './practiceComponent/Age';
import Details from './practiceComponent/Details';
import Details1 from './practiceComponent/Details1';
import Name from './practiceComponent/Name';
import College from './practiceComponent/College';
import LearnTearnery from './practiceComponent/LearnTearnery';
import EffectOnDifferentData from './practiceComponent/EffectOnDifferentData';
import Location from './practiceComponent/Location';
import Images from './practiceComponent/Images';
import Info from './practiceComponent/Info';
import LearnMap from './practiceComponent/LearnMap';
import LearnMap1 from './practiceComponent/LearnMap1';
import ButtonClick from './practiceComponent/ButtonClick';
import LearnUseStateHook1 from './practiceComponent/LearnUseStateHook/LearnUseStateHook1';
import LearnUseStateHook2 from './practiceComponent/LearnUseStateHook/LearnUseStateHook2';
import ItsImage from './practiceComponent/LearnUseStateHook/ItsImage';
import IncrementByChoice from './practiceComponent/IncrementByChoice';
import Toggle from './practiceComponent/LearnUseStateHook/Toggle';
import WhyUseState from './practiceComponent/LearnUseStateHook/WhyUseState';
import Increment from './practiceComponent/LearnUseStateHook/Increment';
import LearnUseEffectHook1 from './practiceComponent/LearnUseEffectHook.jsx/LearnUseEffectHook1';
import Count from './practiceComponent/LearnUseEffectHook.jsx/Count';
import LearnCleanUpFunction from './practiceComponent/LearnUseEffectHook.jsx/LearnCleanUpFunction';
import { createContext, useContext, useState } from 'react';
import MyLinks from './practiceComponent/MyLinks';
import MyRoutes from './practiceComponent/MyRoutes';
import Form1 from './practiceComponent/Form/Form1';
import Form2 from './practiceComponent/Form/Form2';
import NestingRoute from './practiceComponent/NestingRoute';
import ReactRouter from './practiceComponent/ReactRouter';
import Form3 from './practiceComponent/Form/Form3';
import GenderSelection from './practiceComponent/Form/GenderSelection';
import Learn1UseRefHook from './practiceComponent/LearnUseRefHook/Learn1UseRefHook';
import AddToLocalStorage from './practiceComponent/learnLocalStorage/AddToLocalStorage';
import GetLocalStorageData from './practiceComponent/learnLocalStorage/GetLocalStorageData';
import RemoveLocalStorageData from './practiceComponent/learnLocalStorage/RemoveLocalStorageData';
import AddDataToSessionStorage from './practiceComponent/LearnSessionStorage/AddDataToSessionStorage';
import GetDataOfSessionStorage from './practiceComponent/LearnSessionStorage/GetDataOfSessionStorage';
import LearnFormik from './practiceComponent/LearnFormik/LearnFormik';
import FormikTutorial from './practiceComponent/LearnFormik/FormikTutorial';
import AdminRegister from './practiceComponent/webUsers/AdminRegister';
import Parent from './LearnPropDrilling/Parent';

export let context1=createContext()
export let context2=createContext()

function App() {
  let [show,setShow]=useState(true)
let [name,setName]=useState("Prasant")
let [age,setAge]=useState(22)
let [address,setAddress]=useState("Nilkantha-11-Dhading")
//   let a=<p>this is my first paragraph in react</p>
// let name="rammadi"
// let age=22
// let b=`${1+1}`
// console.log(age)
  return (
  <div>
    {/* <p style={{color:"red",backgroundColor:"green"}}>this is paragraph</p>
    <h1 style={{color:"blue",backgroundColor:"red"}}>this heading</h1>

<a href="https://www.facebook.com" target=" ">facebook</a>

<img src="./logo192.png"></img>
 <div className="success">this is div tag</div> 
    {/* hello <div> Ram </div> */}
    {/* {a}
    {name}
    {age} */}
{/* 
<Name></Name>
<Age></Age>
// <Address></Address> */}
{/* // <span className="success">this is div tag</span>
// <div className="error">this is div tag</div>
// <div className="information">this is div tag</div>
// <div className="warning">this is div tag</div> */}
<br></br>
{/* <Details name="prasant" address="tarkeshwor" age={22}> </Details> */}
{/* 

<Details1 name="ram"></Details1>
<College name="patan campus"></College> */}


{/* <LearnTearnery> </LearnTearnery>
<EffectOnDifferentData></EffectOnDifferentData>
<Location country="Nepal" province="bagmati" district="dhading" exactLocation={"nilkantha-11-dhading"}></Location>
<Images></Images>
<Info name="hareram" age={20} fatherDetails={{name:"ram"}} favFood={["mutton"]}></Info>
<LearnMap></LearnMap> */}
{/* <LearnMap1></LearnMap1> */}

{/* <ButtonClick></ButtonClick> */}
{/* <LearnUseStateHook1></LearnUseStateHook1> */}
{/* <LearnUseStateHook2></LearnUseStateHook2> */}
{/* <ItsImage></ItsImage> */}
{/* <Name></Name> */}
{/* <IncrementByChoice></IncrementByChoice> */}
{/* <Toggle></Toggle> */}
{/* <WhyUseState></WhyUseState> */}
{/* <Increment></Increment> */}
{/* <LearnUseEffectHook1></LearnUseEffectHook1> */}
{/* <Count></Count> */}
{/* {show?<LearnCleanUpFunction ></LearnCleanUpFunction>:null} */}
{/* <button onClick={()=>{
  setShow(true)
}}>Show</button><br></br>
<button onClick={()=>{
  setShow(false)
}}>Hide</button> */}
{/* <MyLinks> </MyLinks> */}
{/* <MyRoutes></MyRoutes> */}
{/* <Form1></Form1> */}
{/* <Form2></Form2> */}
{/* <NestingRoute></NestingRoute> */}
{/* <Product></Product> */}
{/* <ReactRouter></ReactRouter> */}
{/* <ReactRouter></ReactRouter> */}
{/* <Form3></Form3> */}
{/* <GenderSelection></GenderSelection> */}
{/* <Learn1UseRefHook></Learn1UseRefHook> */}
{/* <AddToLocalStorage></AddToLocalStorage> */}
{/* <GetLocalStorageData></GetLocalStorageData> */}
{/* <RemoveLocalStorageData></RemoveLocalStorageData> */}
{/* <AddDataToSessionStorage></AddDataToSessionStorage> */}
{/* <GetDataOfSessionStorage></GetDataOfSessionStorage> */}
{/* <LearnFormik></LearnFormik> */}
{/* <FormikTutorial></FormikTutorial> */}
{/* <AdminRegister></AdminRegister> */}


<context1.Provider value={{name:name,age:age,setName:setName,setAge:setAge}}>
  <context2.Provider value={{address:address}}>

  <Parent></Parent>

  </context2.Provider>
</context1.Provider>

     </div>
  );
}

export default App;
