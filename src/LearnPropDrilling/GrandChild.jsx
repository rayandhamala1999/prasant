import React, { useContext } from "react";
import { context1, context2 } from "../App";

const GrandChild = () => {

    let value=useContext(context1)
    let value1=useContext(context2)
    
  return <div>GrandChild <br></br>
    name is {value.name}
    <br></br>
    age is {value.age}
<br></br>
Address is {value1.address}

    <button onClick={()=>{
        value.setName("Rammadi")
    }}>Change name</button>
  </div>;
};

export default GrandChild;
