import React from 'react'
import GrandChild from './GrandChild'
import Child from './Child'

const Parent = () => {
  return (
    <div>Parent 
        <Child>
            
        </Child>
    </div>
  )
}

export default Parent